from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render,get_object_or_404
from django.urls import reverse_lazy , reverse
from .models import Tweet
from django.db.models import Q
from django.views.generic import (
				CreateView,
				DetailView,
				DeleteView,
				ListView,
				UpdateView
				)
from .mixins import FormUserNeededMixin, UserOwnerMixin
from .forms import TweetModelForm
# Create your views here.

#Create 

class TweetCreateView(FormUserNeededMixin, CreateView):
	form_class = TweetModelForm
	template_name = 'tweets/create_view.html'
	#success_url = reverse_lazy("tweet:detail")

	#login_url = '/admin/'

class TweetUpdateView(LoginRequiredMixin , UserOwnerMixin , UpdateView):
	queryset = Tweet.objects.all()
	form_class = TweetModelForm
	template_name = 'tweets/update_view.html'
	#success_url = "/tweet/"

class TweetDeleteView(LoginRequiredMixin , DeleteView):
	model = Tweet
	template_name ='tweets/delete_confirm.html'
	success_url = reverse_lazy("tweet:list")#/tweet/

class TweetDetailView(DetailView):
	queryset = Tweet.objects.all()
	

class TweetListView(ListView):
	def get_queryset(self , *args , **kwargs):

		qs = Tweet.objects.all()
		print(self.request.GET)
		query = self.request.GET.get("q" , NONE)
		if query is not NONE:
			qs = qs.filter(
					Q(content__icontains = query)
					Q(user__username__icontains = query)
					)
		return qs
	
	def get_context_data(self, *args, **kwargs):
		context = super(TweetListView, self).get_context_data(*args,**kwargs)
		return context



def tweet_detail_view(request,pk = None):
#	obj = Tweet.objects.get(id=id)#get from database
	obj = get_object_or_404(Tweet, pk = pk)
	print(obj)
	context={
		"object" : obj
	}
	return render(request,"tweets/detail_view.html",context)

